import urllib
import json
import time
import datetime
import sys
import os

config = json.load(open('config.json'))

RED = "\033[1;31m"
GREEN = "\033[0;32m"
OKBLUE = '\033[94m'

lastprices = {}
trends = {}


def printres(jsondata):
    print "---------------------------------------------------------------"
    print json.dumps(jsondata, sort_keys=True, indent=4).replace("{", "").replace("}", "")


def check_state(config):
    out = {}
    usdsum = 0

    for coin in config['coins']:
        lastprices[coin['coin']] = lastprices.get(coin['coin'], 0)
        trends[coin['coin']] = trends.get(coin['coin'], [])

        url = "https://api.coinmarketcap.com/v1/ticker/" + coin['coin']
        response = urllib.urlopen(url)
        data = json.loads(response.read())[0]

        networth = float(coin['amount']) * float(data['price_usd'])
        usdsum += networth

        if lastprices[coin['coin']] > 0 and lastprices[coin['coin']] > data['price_usd']:
            sys.stdout.write(RED)
            trends[coin['coin']].append("_")
        elif lastprices[coin['coin']] > 0 and lastprices[coin['coin']] < data['price_usd']:
            sys.stdout.write(GREEN)
            trends[coin['coin']].append("-")
        else:
            sys.stdout.write(OKBLUE)

        out[data['name']] = {
            "USD price": {
                "last": float(lastprices[coin['coin']]),
                "current": float(data['price_usd']),
                "change": float(data['price_usd']) - float(lastprices[coin['coin']])
            },
            "market cap usd": data['market_cap_usd'],
            "change (%)": {
                "hourly": data['percent_change_1h'],
                "daily": data['percent_change_24h'],
                "weekly": data['percent_change_7d']
            },
            "money": {
                "in": coin['usd_in'] if 'usd_in' in coin else '',
                "balance": networth,
                "profit": networth - coin['usd_in'] if 'usd_in' in coin else ''
            },
            "trend": "".join(trends[coin['coin']])
        }

        lastprices[coin['coin']] = data['price_usd']

        printres(out)

    if usdsum > config['richness_level_usd']:
        sys.stdout.write(GREEN)
    else:
        sys.stdout.write(RED)

    printres({"AM I RICH YET": "YES :)" if usdsum > int(config["richness_level_usd"]) else "NO :("})


if config["refresh_rate"]:
    while True:
        sys.stdout.write(OKBLUE)
        os.system('clear')
        print "\n==============================================================="
        print datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
        check_state(config)
        print "===============================================================\n"
        time.sleep(int(config['refresh_rate']))




